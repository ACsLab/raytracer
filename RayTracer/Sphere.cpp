#include "Sphere.h"

Sphere::Sphere()
{
}

Sphere::Sphere(float r, glm::vec3  position, glm::vec3  ambiant, glm::vec3  diffuse, glm::vec3 specular, float shinines):Shape(position,ambiant,diffuse,specular,shinines)
{
	this->RADIUS = r;
	this->RADIUS_2 = r * r;
	
}


Sphere::~Sphere()
{
}

float Sphere::getRadius()
{
	return RADIUS;
}

float Sphere::getRadius_2()
{
	return RADIUS_2;
}



bool Sphere::intersec(glm::vec3 rayorigin, glm::vec3 raydir, float & t, int & indices)
{
	
	glm::vec3 o = rayorigin;
	glm::vec3 d = raydir;
	glm::vec3 oc = o - this->pos;
	float a = dot(d, d);
	float b = 2 * glm::dot(oc, d);
	float c = glm::dot(oc, oc) - this->RADIUS_2;
	float disc = b * b - 4 * a*c;
	if (abs(disc) < 1e-8) return false;
	disc = sqrt(disc);
	float t0 = -(b - disc)/2*a;
	float t1 = -(b + disc)/2*a;
	
	t = (t0 < t1) ? t0 : t1;
	glm::vec3 pi = o + d * t;
	glm::vec3 nd = glm::normalize((pi -this->pos)/this->RADIUS);
	if (glm::dot(d, nd) > 0) {
		return false;//backface culling?
	}
		
	return true;
}

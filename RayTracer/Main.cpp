/*
https://www.scratchapixel.com/code.php?id=7&origin=/lessons/3d-basic-rendering/ray-tracing-generating-camera-rays
https://www.scratchapixel.com/code.php?id=3&origin=/lessons/3d-basic-rendering/introduction-to-ray-tracing


*/
#include "../CImg-2.5.5/CImg.h"
#include "../glm/glm.hpp"
#include "Light.h"
#include "Plane.h"
#include "Camera.h"
#include "Renderer.h"
#include "SceneLoader.h"
#include <memory>
using namespace cimg_library;
int main() {

	

	

	std::vector<std::shared_ptr<Shape>> objects;
	/*objects.push_back(sphere);
	objects.push_back(plane);*/
	std::vector<std::shared_ptr<Light>> lights;
	//lights.push_back(light);
	std::shared_ptr<Camera> camera(new Camera());
	//std::vector<int>::iterator it;
	if (!LoadSceneFile("../Scenes_Files/mesh_scene1.txt", camera, objects, lights)) {
		system("pause");
		return -1;
	}
	render(camera,objects,lights);
	objects.clear();
	lights.clear();

	if (!LoadSceneFile("../Scenes_Files/mesh_scene2.txt", camera, objects, lights)) {
		system("pause");
		return -1;
	}
	auto it = objects.begin();
	//objects.erase(++++it);
	render(camera, objects, lights);
	objects.clear();
	lights.clear();

	if (!LoadSceneFile("../Scenes_Files/scene1.txt", camera, objects, lights)) {
		system("pause");
		return -1;
	}
	render(camera, objects, lights);
	objects.clear();
	lights.clear();

	if (!LoadSceneFile("../Scenes_Files/scene2.txt", camera, objects, lights)) {
		system("pause");
		return -1;
	}
	render(camera, objects, lights);
	objects.clear();
	lights.clear();

	if (!LoadSceneFile("../Scenes_Files/scene3.txt", camera, objects, lights)) {
		system("pause");
		return -1;
	}
	render(camera, objects, lights);
	objects.clear();
	lights.clear();

	if (!LoadSceneFile("../Scenes_Files/scene4.txt", camera, objects, lights)) {
		system("pause");
		return -1;
	}
	render(camera, objects, lights);
	objects.clear();
	lights.clear();

	if (!LoadSceneFile("../Scenes_Files/scene5.txt", camera, objects, lights)) {
		system("pause");
		return -1;
	}
	render(camera, objects, lights);

	return 0;
}
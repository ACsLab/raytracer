#pragma once
#include "Sphere.h"

class Light:public Sphere
{

public:
	Light();
	Light(glm::vec3 position, glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular);
	~Light();

	glm::vec3 getLightColor();
};


#pragma once
//tbd if needed or not
#include "../glm/glm.hpp"
class Ray
{
private:
	glm::vec3 origin;
	glm::vec3 direction;
public:
	Ray();
	Ray(glm::vec3 org, glm::vec3 dir);
	~Ray();
	
	void setRayOrigin(glm::vec3 org);
	glm::vec3 getRayOrigin();
	void setRayDirection(glm::vec3 dir);
	glm::vec3 getRayDirection();

};


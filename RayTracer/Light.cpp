#include "Light.h"



Light::Light()
{
}

Light::Light(glm::vec3 position, glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular):Sphere(1,position,ambiant,diffuse,specular,0)
{

}


Light::~Light()
{
}

glm::vec3 Light::getLightColor()
{
	return this->mats.amb+this->mats.dif+this->mats.spec;
}

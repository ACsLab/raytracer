#include "Shape.h"



Shape::Shape()
{
}

Shape::Shape(glm::vec3 position, glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular, float shinines):Object(position)
{
	
	this->mats.amb = ambiant;
	this->mats.dif = diffuse;
	this->mats.spec = specular;
	this->mats.shi = shinines;
}


Shape::~Shape()
{
}

Material Shape::getMats()
{
	return mats;
}

glm::vec3 Shape::getAmb()
{
	return this->getMats().amb;
}

glm::vec3 Shape::getDiff()
{
	return this->getMats().dif;
}

glm::vec3 Shape::getSpec()
{
	return this->getMats().spec;
}

float Shape::getShine()
{
	return this->getMats().shi;
}
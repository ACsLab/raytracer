#pragma once
#include "Shape.h"
class Plane :public Shape
{
protected:
	glm::vec3 norm;
public:
	Plane();
	Plane(glm::vec3 normal, glm::vec3 position, glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular, float shine);
	~Plane();

	glm::vec3 getNorm();
	glm::vec3 getNormal(glm::vec3&, int triIndex);//input is dud
	bool intersec(glm::vec3 rayorig, glm::vec3 raydir, float &t, int & indices);
	/*friend std::ostream &operator<<(std::ostream &os, const Camera  &m) {
		return os << "Plane:\n position: " << pos.x << "," << pos.y << "," << pos.z
			<< "\n norm: " << norm.x << "," << norm.y << "," << norm.z << 
			"\n ambiant"<<"\n diffuse"<<"\n specular"<<"\n shine:"<<;
	}*/
};


#include "Camera.h"




Camera::Camera()
{
}

Camera::Camera(glm::vec3 pos, float fov, float focal_length, float aratio) :Object(pos)
{
	this->fov = fov;
	this->f = focal_length;
	this->aspect_ratio = aratio;
	this->IMG_HEIGHT = 2*(tan(glm::radians(fov) / 2) * f);
	this->IMG_WIDTH = aspect_ratio * IMG_HEIGHT;

}


Camera::~Camera()
{
}

glm::vec3 Camera::castRay(std::shared_ptr<Ray> ray, std::vector<std::shared_ptr<Object>>& object, std::vector<std::shared_ptr<Light>>& lights)
{
	//atm just turns a ray into a color
	glm::vec3 hitColor =  (ray->getRayDirection()+glm::vec3(1)) * 0.5f;


	return hitColor;
}



float Camera::getImgHeight() {
	return IMG_HEIGHT;
}
float Camera::getImgWidth() {
	return IMG_WIDTH;
}

float Camera::getFOV() {
	return fov;
}
float Camera::getAspectRatio() {
	return aspect_ratio;
}

float Camera::getFocalLength()
{
	return f;
}

void Camera::setFOV(float fieldofview)
{
	this->fov = fieldofview;
}

void Camera::setFocalLength(float focallength)
{
	this->f = focallength;
}

void Camera::setAspectRatio(float aratio)
{
	this->aspect_ratio = aratio;
}

void Camera::setPosition(glm::vec3 position)
{
	this->pos = position;
}

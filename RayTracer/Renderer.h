#pragma once
#include "Camera.h"
#include "Light.h"
#include "Sphere.h"
#include <iostream>
#include <typeinfo>
#include "../CImg-2.5.5/CImg.h"
//#define _MAX_DEPTH 1
using namespace cimg_library;



//finds the nearest object to trace
bool trace(glm::vec3 rOrig,glm::vec3 rDir, std::vector<std::shared_ptr<Shape>> &objects,float &tNear,int &index, std::shared_ptr<Shape> &hitObject) {

	tNear = INFINITY;
	//std::vector<std::shared_ptr<Shape>>::const_iterator iter = objects.begin();
	for (int i = 0; i < objects.size();i++) {
		float t = INFINITY;
		int indexTriangle;
		if (objects[i]->intersec(rOrig, rDir, t, indexTriangle) && t < tNear) {
			hitObject = objects[i];
			tNear = t;
			index = indexTriangle;
		}
	}

	return (hitObject != nullptr);
}
//returns the hit objects color
glm::vec3 castRay(std::shared_ptr<Ray> ray,std::shared_ptr<Camera> camera,std::vector<std::shared_ptr<Shape>> &objects, std::vector<std::shared_ptr<Light>> &lights){
	glm::vec3 orig = ray->getRayOrigin(), dir = ray->getRayDirection();
	//glm::vec3 white(255, 255, 255);
	//glm::vec3 red(255, 0, 0);
	glm::vec3 diffuse(0);
	glm::vec3 specular(0);
	glm::vec3 hitColor(0);
	int index = 0;
	std::shared_ptr<Shape> hitObject = nullptr; // this is a pointer to the hit object
	float t; // this is the intersection distance from the ray origin to the hit point
	if (trace(orig, dir, objects, t, index,hitObject)) {
		
		glm::vec3 pi = orig + dir*t;//point of intersection
		glm::vec3 N = glm::normalize(hitObject->getNormal(pi,index));
		
		float cos_theta;
		for (int i = 0; i < lights.size();i++) {
			glm::vec3 L = glm::normalize(lights[i]->getPos() - pi);
			glm::vec3 R = glm::reflect(-L, N);
			float tShadow = INFINITY;
			float bias = 1e-2;
			//float bias = 0;
			std::shared_ptr<Shape> hitObjShadow = nullptr;
			cos_theta = glm::dot(L, N);
			glm::vec3 pointToCamera = glm::normalize(orig - pi);
			if ((!trace(pi + L * bias, -L, objects, tShadow, index,hitObjShadow)) || (hitObject == hitObjShadow) ) {
				//phong ilumination
				diffuse += glm::max(cos_theta, 0.0f) * lights[i]->getDiff() * hitObject->getDiff();
				specular += glm::pow(glm::clamp(glm::dot(pointToCamera,R), 0.0f, 1.0f), hitObject->getShine()) * lights[i]->getSpec() * hitObject->getSpec();
			}
		}
		
		//hitColor = (red + white * glm::vec3(cos_theta)) * glm::vec3(0.5);
		hitColor = hitObject->getAmb()+diffuse+specular;
		
	}

	return hitColor;


}

//main render function
void render(std::shared_ptr<Camera> camera, std::vector<std::shared_ptr<Shape>> &objects, std::vector<std::shared_ptr<Light>> &lights) {
	int IMG_HEIGHT = (int)camera->getImgHeight(), IMG_WIDTH = (int)camera->getImgWidth();
	CImg<float> image(IMG_WIDTH, IMG_HEIGHT, 1, 3, 0); 
	glm::vec3 white(255, 255, 255);
	glm::vec3 black(0, 0, 0);
	glm::vec3 red(255, 0, 0);
	glm::vec3 pix_col(black);
	
	float angle = tan(glm::radians(camera->getFOV() * 0.5));
	
	//main render loop
	for (int y = 0; y < IMG_HEIGHT; ++y) {
		for (int x = 0; x < IMG_WIDTH; ++x) {
			pix_col = black;
			
			//convertin the ray direction
			int relY = IMG_HEIGHT / 2 - y;
			int relX = x + IMG_WIDTH / -2;
			
			glm::vec3 raydir = glm::normalize(glm::vec3(relX, relY, camera->getFocalLength() * -1) - camera->getPos());
			//making the ray
			std::shared_ptr<Ray> ray(new Ray(camera->getPos()+glm::vec3(0,0,-1)*(float)1e-2, raydir));
			
			//coloring the pixel
			pix_col = castRay(ray, camera,objects, lights);
			pix_col = glm::clamp(pix_col, 0.0f, 1.0f);
			
			image(x, y, 0) = pix_col.r * 255.0f;
			image(x, y, 1) = pix_col.g * 255.0f;
			image(x, y, 2) = pix_col.b * 255.0f;

		}
	}


	image.normalize(0, 255);
	image.save("render.bmp");
	CImgDisplay main_disp(image, "Render");
	while (!main_disp.is_closed())
		main_disp.wait();


	
}
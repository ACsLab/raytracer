#pragma once
#include "Object.h"
#include "Materials.h"
class Shape : public Object
{
protected:
	Material mats;
public:
	Shape();
	Shape(glm::vec3 position, glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular, float shinines);
	~Shape();
	virtual bool intersec(glm::vec3,glm::vec3,float &t,int &indices) = 0;
	virtual glm::vec3 getNormal(glm::vec3&, int triIndex)=0;
	glm::vec3 getAmb();
	glm::vec3 getDiff();
	glm::vec3 getSpec();
	float getShine();
	Material getMats();

};


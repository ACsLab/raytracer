#pragma once
#include "Camera.h"
#include "Plane.h"
#include "Sphere.h"
#include "Light.h"
#include "Mesh.h"
#include "objloaderIndex.h"
#include <string>
#include <fstream>
#include <iostream>

glm::vec3 readVector(std::string line) {
	std::string tempS = line.substr(line.find(" ") + 1);
	std::string temp1 = tempS.substr(0, tempS.find(" "));
	tempS = tempS.substr(tempS.find(" ") + 1);
	std::string temp2 = tempS.substr(0, tempS.find(" "));
	tempS = tempS.substr(tempS.find(" ") + 1);
	std::string temp3 = tempS.substr(0, tempS.find(" "));
	return glm::vec3(stof(temp1), stof(temp2), stof(temp3));
}
bool LoadSceneFile(std::string _FILE_LOCATION,std::shared_ptr<Camera> &camera,std::vector<std::shared_ptr<Shape>> &shapes, std::vector<std::shared_ptr<Light>> &lights) {

	std::ifstream infile(_FILE_LOCATION);
	std::string line;
	glm::vec3 normal, position, ambiant, diffuse, specular;
	std::vector<int> indices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> UVs;
	float shine,radius,fov,focal_length,aratio;
	std::string _MESH_FILE_NAME;
	int x;
	if (infile.is_open()) {
		while (getline(infile, line)) {
			if (isdigit(line[0]) == true) {
				std::cout << "Loader found: digit" << std::endl;
				continue;
			}
			else if (line == "camera"){
				std::cout << "Loader found: camera" << std::endl;
				x = 4;
				//std::shared_ptr<Camera> tempcam(new Camera());
				while (x > 0) {
					getline(infile, line);
					if (line.substr(0, line.find(":")) == "pos") {
						position = readVector(line);
						
					}
					else if (line.substr(0, line.find(":")) == "fov") {
						fov = std::stof((line.substr(line.find(" ") + 1)));
						
					}
					else if (line.substr(0, line.find(":")) == "f") {
						 focal_length = std::stof((line.substr(line.find(" ") + 1)));
						
					}
					else if (line.substr(0, line.find(":")) == "a") {
						aratio = std::stof((line.substr(line.find(" ") + 1)));
					}
					x--;
				}
				camera = std::shared_ptr<Camera> (new Camera(position,fov,focal_length,aratio));
				continue;
			}
			else if (line == "plane") {
				std::cout << "Loader found: plane" << std::endl;
				x = 6;
				normal = glm::vec3(0);
				position = glm::vec3(0);
				ambiant = glm::vec3(0);
				diffuse = glm::vec3(0);
				specular = glm::vec3(0);
				shine = 0.0;
			
				while (x > 0) {
					getline(infile, line);
					if (line.substr(0, line.find(":")) == "nor") {
						normal = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "pos") {
						position = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "amb") {
						ambiant = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "dif") {
						diffuse = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "spe") {
						specular = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "shi") {
						float shi = std::stof((line.substr(line.find(" ") + 1)));
						shine = shi;
					}
					x--;
				}
				shapes.push_back(std::shared_ptr<Plane>(new Plane(normal,position,ambiant,diffuse,specular,shine)));
				continue;
			}
			else if (line == "sphere") {
				std::cout << "Loader found: sphere" << std::endl;
				x = 6;
				position = glm::vec3(0);
				ambiant = glm::vec3(0);
				diffuse = glm::vec3(0);
				specular = glm::vec3(0);
				shine = 0.0;
				radius = 0.0;
				while (x > 0) {
					getline(infile, line);
					if (line.substr(0, line.find(":")) == "pos") {
						position = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "rad") {
						radius = std::stof((line.substr(line.find(" ") + 1)));
					
					}
					else if (line.substr(0, line.find(":")) == "amb") {
						ambiant = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "dif") {
						diffuse = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "spe") {
						specular = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "shi") {
						shine = std::stof((line.substr(line.find(" ") + 1)));
					
					}
					x--;
				}
				shapes.push_back(std::shared_ptr<Sphere>(new Sphere(radius,position,ambiant,diffuse,specular,shine)));
				continue;
			}
			else if (line == "light") {
				std::cout << "Loader found: light" << std::endl;
				x = 3;
				position = glm::vec3(0);
				diffuse = glm::vec3(0);
				specular = glm::vec3(0);
				while (x > 0) {
					getline(infile, line);
					if (line.substr(0, line.find(":")) == "pos") {
						position = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "dif") {
						diffuse = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "spe") {
						specular = readVector(line);
					}
					x--;
				}
				lights.push_back(std::shared_ptr<Light>(new Light(position,glm::vec3(0,0,0),diffuse,specular)));
				continue;
			}
			else if (line == "mesh") {
				std::cout << "Loader found: mesh" << std::endl;
				x = 5;
				while (x > 0) {
					getline(infile, line);
					if (line.substr(0, line.find(":")) == "file") {
						_MESH_FILE_NAME.append((line.substr(line.find(" ") + 1)));
						//position = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "amb") {
						ambiant = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "dif") {
						diffuse = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "spe") {
						specular = readVector(line);
					}
					else if (line.substr(0, line.find(":")) == "shi") {
						shine = std::stof((line.substr(line.find(" ") + 1)));

					}
					x--;
				}
				//your file path here
				int found = _FILE_LOCATION.find_last_of("/");
				if (found != std::string::npos) {
					_FILE_LOCATION.replace(found + 1, _FILE_LOCATION.size(), _MESH_FILE_NAME);
				}
				//std::string _FILE_PATH = "../Scenes_Files/"; 
				//_FILE_PATH.append(_MESH_FILE_NAME);
				const char *cstr = _FILE_LOCATION.c_str();
				loadOBJ(cstr, indices, vertices, normals, UVs);//loading object
				/*for (int i = 0; i < indices.size(); i++) {
					std::cout << indices[i]<<", ";
				}*/
				shapes.push_back(std::shared_ptr<Mesh>(new Mesh(indices, vertices, ambiant, diffuse, specular, shine)));
			}

		}
	
	}else {
		std::cout << "Unable to open file" << std::endl;
		return false;
	}
	
	return true;
}


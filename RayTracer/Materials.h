#pragma once
#include "../glm/glm.hpp"
//Shape Materials
struct Material {
	glm::vec3 amb;
	glm::vec3 dif;
	glm::vec3 spec;
	float shi;
};
//light color data
struct Light_Color_Data {
	glm::vec3 amb;
	glm::vec3 dif;
	glm::vec3 spec;
};
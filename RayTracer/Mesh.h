#pragma once
//tbd if required 
//i.e can use old obj loader.
//might use in conjunction
#include "Shape.h"
#include <vector>
class Mesh:public Shape
{
private:
	std::vector<int> indices;
	std::vector<glm::vec3> vertices;

public:
	Mesh();
	Mesh(std::vector<int>,std::vector<glm::vec3>, glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular, float shinines);
	~Mesh();
	bool rayTriangleIntersect(
		glm::vec3 &orig, glm::vec3 &dir,
		glm::vec3 &v0, glm::vec3 &v1, glm::vec3 &v2,float &t);
	bool intersec(glm::vec3, glm::vec3, float &t, int &triIntersec); 
	glm::vec3 getNormal(glm::vec3&, int triIndex);
};


#include "Mesh.h"



Mesh::Mesh()
{
}

Mesh::Mesh(std::vector<int> ind, std::vector<glm::vec3> verts, glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular, float shinines):
	Shape(glm::vec3(0),ambiant,diffuse,specular,shinines)
{
	this->indices = ind;
	this->vertices = verts;
}


Mesh::~Mesh()
{
}

bool Mesh::rayTriangleIntersect(glm::vec3 & orig, glm::vec3 & dir, glm::vec3 & v0, glm::vec3 & v1, glm::vec3 & v2, float & t)
{
	float u, v;
	//getting the direction vectors of the verteces 
	glm::vec3 v0v1 = v1 - v0;
	glm::vec3 v0v2 = v2 - v0;
	glm::vec3 pvec = glm::cross(dir,v0v2);
	float det = glm::dot(v0v1,pvec);

	if (det < 1e-8) return false;//backface culling?
	if (glm::dot(dir, normalize(cross(v0v1, v0v2))) > 0)return false;
	// ray and triangle are parallel if det is close to 0
	//if (fabs(det) < 1e-8) return false;

	float invDet = 1 / det;

	glm::vec3 tvec = orig - v0;
	u = glm::dot(tvec, pvec)* invDet;// tvec.dotProduct(pvec) * invDet;
	if (u < 0 || u > 1) return false;

	glm::vec3 qvec = glm::cross(tvec,v0v1)/*tvec.crossProduct(v0v1)*/;
	v = glm::dot(dir, qvec)* invDet;// dir.dotProduct(qvec) * invDet;
	if (v < 0 || u + v > 1) return false;

	t = glm::dot(v0v2,qvec) * invDet;

	return true;
}

bool Mesh::intersec(glm::vec3 orig, glm::vec3 dir, float & tNear,int &triIndex)
{
	//calculating the intersection of a specific triangle 
	int j = 0;
	bool isect = false;
	int numTris = this->indices.size() / 3;
	for (int i = 0; i < numTris; ++i) {
		//building the indexed trinagle
		glm::vec3 &v0 = this->vertices[this->indices[j]];
		glm::vec3 &v1 = this->vertices[this->indices[j+1]];
		glm::vec3 &v2 = this->vertices[this->indices[j+2]];
		float t = INFINITY, u, v;
		//chacking if the ray intersects with the triangle
		if (rayTriangleIntersect(orig, dir, v0, v1, v2, t) && t < tNear) {
			//if it intersects set triIndex to the intersection index and t near to the intersection distance
			tNear = t;
			triIndex = i;
			//set the intersection to true
			isect = true;
		}
		j += 3;
	}

	return isect;
}


glm::vec3 Mesh::getNormal(glm::vec3 &, int triIndex)
{

	// face normals
	glm::vec3 &v0 = vertices[indices[triIndex * 3]];
	glm::vec3 &v1 = vertices[indices[triIndex * 3 + 1]];
	glm::vec3 &v2 = vertices[indices[triIndex * 3 + 2]];
	glm::vec3 hitNormal = glm::cross(v1 - v0, v2 - v0);
	
	return glm::normalize(hitNormal);
}

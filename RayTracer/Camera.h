#pragma once
//#include "Object.h"
#include "Light.h"
#include "Ray.h"
#include <vector>
#include <memory>
class Camera:public Object
{
private:
	float fov;
	float f;//far?
	float aspect_ratio;//aspect ratio
	float IMG_HEIGHT;
	float IMG_WIDTH;
public:
	Camera();
	Camera(glm::vec3 pos,float fov, float focal_length, float aratio);
	~Camera();

	glm::vec3 castRay(std::shared_ptr<Ray> ray, std::vector<std::shared_ptr<Object>> &object, std::vector<std::shared_ptr<Light>> &lights);//look at trace method
	
	float getImgHeight();
	float getImgWidth();
	float getFOV();
	float getAspectRatio();
	float getFocalLength();
	void setFOV(float);
	void setFocalLength(float);
	void setAspectRatio(float);
	void setPosition(glm::vec3);
	/*friend std::ostream &operator<<(std::ostream &os, const Camera  &m) {
		return os << "Cmera:\n position: " << pos.x << "," << pos.y << "," << pos.z
			<<"\n fov: "<<fov<<"\n focal_length: "<<f<<"\n aspect_ratio: "<< aspect_ratio;
	}*/
};


#include "Ray.h"



Ray::Ray()
{
}

Ray::Ray(glm::vec3 org, glm::vec3 dir)
{
	this->origin = org;
	this->direction = dir;

}


Ray::~Ray()
{
}

void Ray::setRayOrigin(glm::vec3 org)
{
	this->origin = org;
}

glm::vec3 Ray::getRayOrigin()
{
	return origin;
}

void Ray::setRayDirection(glm::vec3 dir)
{
	this->direction = dir;
}

glm::vec3 Ray::getRayDirection()
{
	return direction;
}

#include "Plane.h"



Plane::Plane()
{
}

Plane::Plane(glm::vec3 normal,glm::vec3 position, glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular, float shine):Shape(position,ambiant,diffuse,specular,shine)
{
	this->norm = normal;

}


Plane::~Plane()
{
}

glm::vec3 Plane::getNorm()
{
	return norm;
}

glm::vec3 Plane::getNormal(glm::vec3 &, int triIndex)
{
	return norm;
}

bool Plane::intersec(glm::vec3 rayorig, glm::vec3 raydir, float & t, int & indices)
{
	float denom = glm::dot(this->norm, raydir);
	if (abs(denom) > 1e-6) {
		glm::vec3 p0l0 = this->pos - rayorig;
		t = glm::dot(p0l0, this->norm) / denom;
		return (t >= 0);
	}

	return false;
}

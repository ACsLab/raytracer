#pragma once
#include "Shape.h"
#include <cmath>

#define _USE_MATH_DEFINES
#include <math.h>
class Sphere:public Shape
{
protected:
	float RADIUS,RADIUS_2;
	//glm::vec3 center;//may or may not be needed i.e center = pos
public:
	Sphere();
	Sphere(float r, glm::vec3 position, glm::vec3 ambiant, glm::vec3 diffuse, glm::vec3 specular, float shinines);
	~Sphere();

	float getRadius();
	float getRadius_2();
	glm::vec3 getNormal(glm::vec3& pi, int triIndex){ return (pi - this->pos) / this->RADIUS; }
	//glm::vec3 getCenter();
	/*glm::vec3 getAmb();
	glm::vec3 getDiff();
	glm::vec3 getSpec();
	double getShine();*/


	bool intersec(glm::vec3 rayorigin,glm::vec3 raydir, float &t, int & indices);
};


#pragma once
#include "../glm/glm.hpp"
class Object
{
protected:
	glm::vec3 pos;
	
public:
	Object();
	Object(glm::vec3);
	~Object();

	glm::vec3 getPos();
	/*friend std::ostream &operator<<(std::ostream &os, const Object  &m) {
		return os << "Object:\n position:" << pos.x << "," << pos.y << "," << pos.z;
	}*/
};

